import React, { Component } from 'react';
import { StyleSheet,ScrollView, Text, View, FlatList, TouchableOpacity, StatusBar, Image, SafeAreaView, Alert } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';
import styles from './style';

const appname = 'GetBarry'

export default class Consumption extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    //components did mount
    componentDidMount() {

        this.props.fetchdata(this.props.currentdate)
    }

    //loader rendering    
    loader() {
        if (this.props.isLoading == true) {
            return (
                <View>
                    <Spinner visible={this.props.isLoading} textContent={""} color="#FFFFFF" />
                </View>
            )
        }
    }

    // Convert date formate to 'DD/MM YYYY'
    formatedate(newdate) {
        return moment(newdate).utc().format('DD/MM YYYY')
    }

    // Alert data on pop up with formate date
    alertdata(newdate, newvalue) {
        if (this.props.activetab == 'Kwh') {
            Alert.alert(appname, "You have consumed " + newvalue + " Kwh on " + newdate);
        } else if (this.props.activetab == 'Kr') {
            Alert.alert(appname, "You consumed " + newvalue + " Kr charge on " + newdate);
        }
    }

    // Convert Month name and year from date
    monthname(monthdate) {
        return moment(monthdate).utc().format('MMMM YYYY');
    }

    // Second sectino of Total Count view 
    totalcount(){

        return(
            <View style={styles.avarageview}>
            <Text style={{
                fontSize: 32, fontWeight: 'bold', lineHeight: 38, alignItems: 'center', display: 'flex',
                color: (this.props.activetab == "Kwh") ? "#FF4B9B" : "#EF7B40"
            }}>
                {(this.props.activetab == "Kwh") ? this.props.kwhtotal : this.props.krtotal}
                <Text style={{ fontSize: 16, color: '#fff', opacity: 0.6, fontWeight: 'bold' }}> {(this.props.activetab == 'Kwh') ? "kWh" : "Kr"}</Text>
            </Text>
        </View>

        ) 
    }

    // Dispaly No data found view
    nodatafound(){
        return(
        <View style={{ alignItems: 'center', justifyContent: 'center', height:300 }}>
        <Text style={{ fontSize: 15, color: '#ffffff' }}>{this.props.error}</Text>
         </View>
        )
    } 



    // Get and display second section using Flatlist 
    listdata() {

        //console.log("Data come", this.props.data.result)
        return (
            
            <View style={styles.secondview}>
                <View style={styles.mainlistview}>
                    <FlatList
                         ListHeaderComponent={ this.totalcount()}
                        data={this.props.data.result}
                        refreshing={this.props.isLoading}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => (

                            <TouchableOpacity onPress={() => (this.props.activetab == 'Kwh') ? this.alertdata(this.formatedate(item.date), item.value.toFixed(2)) : this.alertdata(this.formatedate(item.date), item.priceIncludingVat.toFixed(2))} style={styles.singlelistview}>
                                <View style={styles.dateview}>
                                    <Text style={styles.datetext}>{this.formatedate(item.date)}</Text>
                                </View>
                                <View style={styles.dipslayValueview} >
                                    <Text style={styles.valuetext}>
                                        {(this.props.activetab == 'Kwh') ? item.value.toFixed(2) : item.priceIncludingVat.toFixed(2)}
                                        <Text style={{ fontSize: 10, opacity: 0.8, fontWeight: 'bold' }} > {(this.props.activetab == 'Kwh') ? ' Kwh' : ' Kr'}  </Text> <Image source={require('../assets/images/unitarrow.png')} style={{ width: 6, height: 9 }} resizeMode="contain" />
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        )}
                        ListEmptyComponent={this.nodatafound()}
                    />
                </View>

            </View>
          

        )

    }



// Render Design 
    render() {

        console.log("data is", this.props);

        return (

            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor="#2C2F36" barStyle="light-content" />
                <View style={{ alignItems: 'center' }}>
                    {this.loader()}
                </View>

                <View style={styles.topsectionview}>
                    <View style={styles.titleview}>
                        <Text style={styles.titletext}> Your consumption</Text>
                    </View>

                    <View style={styles.topmidsectionview}>

                        <View style={styles.previosArrowview}>
                            <TouchableOpacity style={{ width: 9, height: 16 }} onPress={() => this.props.fetchdata(this.props.currentdate ,'prev')}>
                                <Image source={require('../assets/images/previousarrow.png')} style={{ width: 9, height: 16 }} resizeMode="contain" />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.monthNameview}>
                            <Text style={styles.monthtext} >{this.monthname(this.props.currentdate)} </Text>
                        </View>

                        <View style={styles.nextArrowview}>
                            <TouchableOpacity style={{ width: 9, height: 16 }} onPress={() => this.props.fetchdata(this.props.currentdate,'next')} >
                                <Image source={require('../assets/images/nextarrow.png')} style={{ width: 9, height: 16 }} resizeMode="contain" />

                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.topbottomsectionview}>
                        <View style={styles.buttonview}>
                            <TouchableOpacity style={{ width: 160, height: 35, alignItems: 'center', justifyContent: 'center', borderRadius: 18, backgroundColor: (this.props.activetab == "Kwh") ? "#FF4B9B" : "#32363D" }} onPress={() => this.props.tab('Kwh')} >
                                <Text style={styles.buttontext}>kWh</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ width: 160, height: 35, alignItems: 'center', justifyContent: 'center', borderRadius: 18, backgroundColor: (this.props.activetab == "Kr") ? "#EF7B40" : "#32363D" }} onPress={() => this.props.tab('Kr')}>
                                <Text style={styles.buttontext}>kr.</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>

                {this.listdata()}

            </SafeAreaView>

        );
    }
}