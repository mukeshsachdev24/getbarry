import {StyleSheet} from 'react-native';

// Style for this component
const styles = StyleSheet.create({
    container: {
        flex: 10,
        justifyContent: 'center',
        //alignItems: 'center',
        backgroundColor: '#2C2F36',
    },
    topsectionview: {
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#05232d',
        /*shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 1.0,
        shadowRadius: 2,  */
        elevation: 2,
        borderColor: '#FFFFFF',
        borderWidth: 0,
        paddingBottom: 10
    },
    secondview: {
        flex: 7,
        borderColor: '#FFFFFF',
        borderWidth: 0,
        // justifyContent:'center',
        // alignItems:'center', 
    },
    avarageview: {
        paddingTop:10,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#FFFFFF',
        borderWidth: 0,
    },
    buttonview: {
        flexDirection: 'row',
        backgroundColor: '#32363D',
        borderRadius: 18,
    },
    buttontext: {
        fontSize: 12,
        lineHeight: 16,
        color: '#ffffff',
        fontWeight: '800'
    },
    mainlistview: {
        flex: 6,
    },
    unitText: {
        fontSize: 16,
        lineHeight: 19,
        fontWeight: 'bold',
        opacity: 0.6,
        color: '#ffffff'
    },
    singlelistview: {
        flexDirection: 'row',
        height: 60,
        flex: 10,
        // alignItems:'center',
        //borderColor:'#FFFFFF',
        borderWidth: 0,
        borderBottomColor: '#373940',
        borderBottomWidth: 1,
    },
    dipslayValueview: {
        flex: 0.3,
        borderColor: '#FFFFFF',
        borderWidth: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    dateview: {
        flex: 0.7,
        justifyContent: 'center',
        alignItems: 'flex-start',
        borderColor: '#FFFFFF',
        borderWidth: 0,
        paddingLeft: 30
    },
    kwhButton: {
        width: 160,
        height: 35,
        //   backgroundColor: '#FF4B9B',
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 18
    },
    krButton: {
        width: 160,
        height: 35,
        //   backgroundColor: '#32363D',
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 18
    },
    datetext: {
        color: '#FFFFFF',
        fontSize: 14,
        lineHeight: 17,
        fontWeight: 'bold',
        alignItems: 'flex-start'
    },
    valuetext: {
        color: '#FFFFFF',
        fontSize: 14,
        lineHeight: 17,
        fontWeight: 'bold',
        alignItems: 'flex-end'
    },
    rangetitle: {
        fontSize: 32,
        fontWeight: 'bold',
        lineHeight: 38,
        alignItems: 'center',
        display: 'flex',
    },
    titleview: {
        flex: 1,
        borderColor: '#000000',
        borderWidth: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titletext: {
        fontWeight: "600",
        fontSize: 18,
        lineHeight: 21,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        color: '#FFFFFF'
    },
    topmidsectionview: {
        flex: 1,
        borderColor: '#000000',
        borderWidth: 0,
        flexDirection: 'row',
        alignItems: 'center',
    },
    previosArrowview: {
        flex: 0.2,
        paddingLeft: 20
    },
    monthNameview: {
        flex: 0.6
    },
    nextArrowview: {
        flex: 0.2,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        paddingRight: 20

    },
    topbottomsectionview: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: '#000000',
        borderWidth: 0,
    },
    monthtext: {
        fontWeight: "600",
        fontSize: 18,
        lineHeight: 21,
        display: 'flex',
        alignItems: 'center',
        textAlign: 'center',
        color: '#FFFFFF'
    },
    previosArrowtext: {
        fontSize: 15,
        lineHeight: 18,
        color: '#FFFFFF',
        alignItems: 'center',
        textAlign: 'center',
    },
    nextArrowtext: {
        fontSize: 15,
        lineHeight: 18,
        color: '#FFFFFF',
        alignItems: 'center',
        textAlign: 'center',
    },
    tabview: {
        flexDirection: 'row',
        flex: 8
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

export default styles;
