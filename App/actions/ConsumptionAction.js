import { connect } from 'react-redux';
import * as ActionTypes from './ActionTypes';
import ConsumptionComponent from '../components/consumptionComponent';
import moment from 'moment';


// Connect data to the Component  
const mapStateToProps = (state) => ({
    isLoading: state.consumptionReducer.isLoading,
    error: state.consumptionReducer.error,
    data: state.consumptionReducer.data,
    krtotal: state.consumptionReducer.krtotal,
    kwhtotal: state.consumptionReducer.kwhtotal,
    activetab: state.consumptionReducer.activetab,
    currentdate: state.consumptionReducer.currentdate
});


// Dispact to action 
const mapDispatchToProps = (dispatch) => ({

    tab : (currenttab) => {
        dispatch(selectTab(currenttab))
    },
    fetchdata: (currentdate,args) => {
        dispatch(fetchconsumptionData(currentdate,args))
    }
});


export const selectTab = (selectedtab) => {
console.log("Tab",selectedtab);
return dispatch => {
var activetab = selectedtab;
return dispatch(selectedTab(activetab))
}
}

// Pass active tab parameter to reducer 
export const selectedTab = (activetab) => ({
    type: ActionTypes.ACTIVE_TAB,
    activetab: activetab
})

// Get Data from API by passing parameter and dispatch to other funciton based on API response
export const fetchconsumptionData = (newdate, args) => {

    if(args == "prev"){
        var currentdate = moment(newdate).subtract(1, 'M');
    }else if(args == "next"){
        var currentdate = moment(newdate).add(1, 'M');
    }else{
        var currentdate = newdate
    }
 
    return dispatch => {

        dispatch(getData())

        const oldstartdate = moment(currentdate).startOf('month').utc();
        const oldenddate = moment(currentdate).endOf('month').utc();

        oldstartdate.set({ hour: 4, minute: 30, second: 0, millisecond: 0 })
        oldstartdate.toISOString()
        oldstartdate.format()

        oldenddate.set({ hour: 4, minute: 30, second: 0, millisecond: 0 })
        oldenddate.toISOString()
        oldenddate.format()
     
        const startdate = moment(oldstartdate).add(2, 'd').utc();
        const enddate = moment(oldenddate).add(1, 'd').utc();
     

        return fetch('https://jsonrpc.getbarry.dk/json-rpc', {
            method: 'POST',
            body: JSON.stringify({
                "jsonrpc": "2.0",
                "id": "1",
                "method": "co.getbarry.megatron.controller.ConsumptionFreemiumController.getDailyConsumptionWithPrice",
                "params": [startdate, enddate, "CET"]
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {

                var kwhtotal = responseJson.result.reduce((accumulator, currentValue) => accumulator + currentValue.value, 0).toFixed(2);
                var krtotal = responseJson.result.reduce((accumulator, currentValue) => accumulator + currentValue.priceIncludingVat, 0).toFixed(2);

                dispatch(getDataSuccess(responseJson, kwhtotal, krtotal,currentdate))

            })
            .catch((error) => {
                console.error(error);
                dispatch(getDataFailure(error))
            });
    }
}


export const getData = () => ({
    type: ActionTypes.FETCH_DATA
})

// Pass API data with other parameter to reducer
export const getDataSuccess = (data, kwhtotal, krtotal,currentdate) => ({
    type: ActionTypes.FETCH_DATA_SUCCESS,
    data: data,
    kwhtotal: kwhtotal,
    krtotal: krtotal,
    currentdate: currentdate
})


// Pass error message to reducer
export const getDataFailure = (error) => ({

    type: ActionTypes.FETCH_DATA_FAILURE,
    error: error

})

export default connect(mapStateToProps, mapDispatchToProps)(ConsumptionComponent);