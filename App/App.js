import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './reducers/index';
import  ConsumptionActions from './actions/ConsumptionAction';

export default class App extends Component {
  constructor(props){
        super(props);
    }

    render() {
        return (
            <Provider store={store}> 
             <ConsumptionActions />
             </Provider>
        );
    }
}