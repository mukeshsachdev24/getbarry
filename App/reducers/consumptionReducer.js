import * as Actions from '../actions/ActionTypes';



// Set to initial state and update state by action called
const consumptionReducer = (state = { isLoading: '', error: "No data available", data: {}, kwhtotal: '', krtotal: '',currentdate: new Date() , activetab:'Kwh'}, action) => {
    switch (action.type) {
        case Actions.FETCH_DATA:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case Actions.FETCH_DATA_FAILURE:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case Actions.FETCH_DATA_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                data: action.data,
                kwhtotal: action.kwhtotal,
                krtotal: action.krtotal,
                currentdate: action.currentdate
            });
        case Actions.ACTIVE_TAB:
                return Object.assign({}, state, {
                    isLoading: false,
                    activetab: action.activetab
                });
        default:
            return state;
    }
}
export default consumptionReducer;