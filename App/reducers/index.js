import { combineReducers, applyMiddleware, createStore, compose } from 'redux';
import thunk from 'redux-thunk';
import consumptionReducer from './consumptionReducer';

const AppReducers = combineReducers({
    consumptionReducer,
});

const rootReducer = (state, action) => {
    return AppReducers(state,action);
}

let store = createStore(rootReducer,compose(applyMiddleware(thunk)));

export default store;