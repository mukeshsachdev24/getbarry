Name of project
=================================================
GetBarry Assignment 

Version of React Native Used
=================================================
React native Version : "0.61.5",
React : "16.9.0"

Installed Required packaged with version
================================================
 moment : "^2.24.0",
 react-redux : "7.1.3",
 redux : "4.0.5",
 redux-thunk: "2.3.0"
 react-native-loading-spinner-overlay: "1.0.1",

Configuration instructions
===================================================
Require below packaged to need to install in system
1) Node.js  
2) React native 

How to Install and Run project 
================================================
Please follow the below step for command line 
1) copy project directory with files in your system and install npm
2) Navigate project directory using command prompt (cd folder name) 
3) Start the react-native metro server by using command "npm start" or "react-native start"
4) Run Project in Android then use command "react-native run-android"
5) Run Project in iOS then use command "react-native run-ios"
6) You can run project by using Android studio and Xcode also

How to Work
=================================================
1) Initial Display Consumption data of current month 
2) You can get next and previos month data by click Next and Previos arrow.
3) You can switch consumption power and rate by click Tab option.
4) When you click particular consumption data then it will display in popup. 
5) Display Message "Data not available" if API response is empty.






